#ifndef MPI_PI_GPU_KERNELS_H
#define MPI_PI_GPU_KERNELS_H

#include "constants.h"

//--------------------------------------------------------------------------------------------------
void init(double *arr, int gpu_idx);

//--------------------------------------------------------------------------------------------------
void reduce(double *arr);

#endif
