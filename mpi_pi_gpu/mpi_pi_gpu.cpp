// Neil Gershenfeld 10/14/20
// Erik Strand 3/6/2021
// calculation of pi by a CUDA+MPI sum
// assumes one GPU per MPI rank

#include <chrono>
#include <cuda_runtime.h>
#include <iostream>
#include <mpi.h>
#include "constants.h"
#include "kernels.h"

using namespace std;

int const n_loops = 8;

int main(int argc, char** argv) {
    // Determine our index within this node.
    char* local_rank_str = nullptr;
    int local_rank = 0;
    local_rank_str = getenv("SLURM_LOCALID");
    if (local_rank_str != nullptr) {
        local_rank = atoi(local_rank_str);
    } else {
        std::cerr << "slurm local rank not defined\n";
    }

    // Determine how many GPUs this node has.
    int n_gpus;
    cudaGetDeviceCount(&n_gpus);
    // local meaning relative to this node
    int local_gpu_idx = local_rank % n_gpus;
    cudaSetDevice(local_gpu_idx);

    // Initialize MPI.
    int n_tasks, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &n_tasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // We assume one GPU per rank.
    int gpu_idx = rank;

    /*
    // Check what node we're on.
    int host_name_length;
    char host_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(host_name, &host_name_length);
    std::cout << "task: " << rank << " of " << n_tasks << '\n';
    std::cout << "node: " << host_name << '\n';
    std::cout << "slurm local id: " << local_rank << '\n';
    std::cout << "local gpu idx: " << local_gpu_idx << " of " << n_gpus << '\n';
    std::cout << "global gpu idx: " << gpu_idx << '\n';
    std::cout << '\n';
    */

    // allocate device data
    double *d_arr;
    cudaMalloc(&d_arr, n_threads_per_gpu * sizeof(double));

    // host data
    double result, pi;

    // rank 0 timing data
    decltype(std::chrono::high_resolution_clock::now()) global_start;
    decltype(std::chrono::high_resolution_clock::now()) global_stop;
    decltype(std::chrono::high_resolution_clock::now()) start;
    decltype(std::chrono::high_resolution_clock::now()) stop;

    if (rank == 0) {
        global_start = std::chrono::high_resolution_clock::now();
    }
    for (int i = 0; i < n_loops; ++i) {
        if (rank == 0) {
            start = std::chrono::high_resolution_clock::now();
        }

        MPI_Barrier(MPI_COMM_WORLD);
        init(d_arr, gpu_idx);
        reduce(d_arr);
        cudaDeviceSynchronize();
        cudaMemcpy(&result, d_arr, sizeof(double), cudaMemcpyDeviceToHost);
        MPI_Reduce(&result, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank == 0) {
            stop = std::chrono::high_resolution_clock::now();
            auto const duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
            auto const millis = duration.count();
            auto const n_terms_total = n_tasks * n_terms_per_gpu;
            auto const gflops = n_terms_total * 5.0 / (millis * 1e-3) * 1e-9;

            std::cout << "loop " << i << '\n';
            std::cout << "processes = " << n_tasks << ", terms per GPU = " << n_terms_per_gpu
                << ", total terms = " << n_terms_total << '\n';
            std::cout << "time = " << millis * 1e-3 << "s, estimated GFlops = " << gflops << '\n';
            std::cout << "pi ~ " << pi << '\n';
            std::cout << '\n';
        }
    }
    if (rank == 0) {
        global_stop = std::chrono::high_resolution_clock::now();
        auto const duration = std::chrono::duration_cast<std::chrono::milliseconds>(global_stop - global_start);
        auto const millis = duration.count();
        std::cout << "total time = " << millis * 1e-3 << "s\n";
    }

    cudaFree(d_arr);
    MPI_Finalize();
    return 0;
}
