#include "kernels.h"

//--------------------------------------------------------------------------------------------------
__global__
void init_kernel(double *arr, int gpu_idx) {
    uint64_t const thread_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_idx >= n_threads_per_gpu) {
        return;
    }

    uint64_t const start = n_terms_per_gpu * gpu_idx + n_terms_per_thread * thread_idx + 1;
    uint64_t const end = start + n_terms_per_thread;

    double sum = 0.0;
    uint64_t i = start;
    while (i < end) {
        #pragma unroll
        for (int j = 0; j < 10; ++j) {
            sum += 0.5 / ((i - 0.75) * (i - 0.25));
            ++i;
        }
    }
    arr[thread_idx] = sum;
}

//--------------------------------------------------------------------------------------------------
__global__
void reduce_sum_kernel(double *arr, uint64_t stride) {
    uint64_t const thread_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_idx < stride) {
        arr[thread_idx] += arr[thread_idx + stride];
    }
}

//..................................................................................................
void init(double *arr, int gpu_idx) {
    init_kernel<<<n_blocks_per_gpu, n_threads_per_block>>>(arr, gpu_idx);
}

//..................................................................................................
void reduce(double *arr) {
    uint64_t stride = n_threads_per_gpu >> 1;
    while (stride > 0) {
        reduce_sum_kernel<<<n_blocks_per_gpu, n_threads_per_block>>>(arr, stride);
        stride = stride >> 1;
    }
}
