// Neil Gershenfeld 3/1/20
// Erik Strand 3/19/2021
// FLOPS test on one GPU using CUDA
// based on a series expansion of pi, but ignoring numeric concerns

#include <chrono>
#include <iostream>

using namespace std;

// currently init_kernel assumes n_terms_per_thread is a multiple of 10
uint64_t const n_terms_per_thread = 1000000;
uint64_t const n_threads_per_gpu = 1024 * 1024;
uint64_t const n_terms_per_gpu = n_terms_per_thread * n_threads_per_gpu;
uint64_t const n_threads_per_block = 1024;
uint64_t const n_blocks_per_gpu = (n_threads_per_gpu + n_threads_per_block - 1) / n_threads_per_block;

int const n_loops = 8;

//--------------------------------------------------------------------------------------------------
__global__
void init_kernel(double *arr) {
    uint64_t const thread_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_idx >= n_threads_per_gpu) {
        return;
    }

    uint64_t const start = n_terms_per_thread * thread_idx + 1;
    uint64_t const end = start + n_terms_per_thread;

    double sum = 0.0;
    uint64_t i = start;
    while (i < end) {
        #pragma unroll
        for (int j = 0; j < 10; ++j) {
            sum += 0.5 / ((i - 0.75) * (i - 0.25));
            ++i;
        }
    }
    arr[thread_idx] = sum;
}

//--------------------------------------------------------------------------------------------------
__global__
void reduce_sum_kernel(double *arr, uint64_t stride) {
    uint64_t const thread_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_idx < stride) {
        arr[thread_idx] += arr[thread_idx + stride];
    }
}

//--------------------------------------------------------------------------------------------------
void init(double *arr) {
    init_kernel<<<n_blocks_per_gpu, n_threads_per_block>>>(arr);
}

//--------------------------------------------------------------------------------------------------
void reduce(double *arr) {
    uint64_t stride = n_threads_per_gpu >> 1;
    while (stride > 0) {
        reduce_sum_kernel<<<n_blocks_per_gpu, n_threads_per_block>>>(arr, stride);
        stride = stride >> 1;
    }
}

//--------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {
    // allocate device data
    double *d_arr;
    cudaMalloc(&d_arr, n_threads_per_gpu * sizeof(double));

    // host data
    double result;

    // timing data
    decltype(std::chrono::high_resolution_clock::now()) global_start;
    decltype(std::chrono::high_resolution_clock::now()) global_stop;
    decltype(std::chrono::high_resolution_clock::now()) start;
    decltype(std::chrono::high_resolution_clock::now()) stop;

    global_start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < n_loops; ++i) {
        start = std::chrono::high_resolution_clock::now();

        init(d_arr);
        reduce(d_arr);
        cudaDeviceSynchronize();
        cudaMemcpy(&result, d_arr, sizeof(double), cudaMemcpyDeviceToHost);

        stop = std::chrono::high_resolution_clock::now();
        auto const duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        auto const millis = duration.count();
        auto const gflops = n_terms_per_gpu * 5.0 / (millis * 1e-3) * 1e-9;

        std::cout << "loop " << i << '\n';
        std::cout << "processes = " << 1 << ", terms per GPU = " << n_terms_per_gpu << '\n';
        std::cout << "time = " << millis * 1e-3 << "s, estimated GFlops = " << gflops << '\n';
        std::cout << "pi ~ " << result << '\n';
        std::cout << '\n';
    }
    global_stop = std::chrono::high_resolution_clock::now();
    auto const duration = std::chrono::duration_cast<std::chrono::milliseconds>(global_stop - global_start);
    auto const millis = duration.count();
    std::cout << "total time = " << millis * 1e-3 << "s\n";

    cudaFree(d_arr);
    return 0;
}
