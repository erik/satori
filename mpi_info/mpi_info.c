#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    char* local_rank_str = NULL;
    int local_rank = 0;
    local_rank_str = getenv("SLURM_LOCALID");
    if (local_rank_str != NULL) {
        local_rank = atoi(local_rank_str);
        printf("slurm local rank = %d\n", local_rank);
    } else {
        printf("slurm local rank not defined\n");
    }

    int n_tasks, rank, length;
    char host_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &n_tasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(host_name, &length);
    printf("Number of tasks = %d, rank = %d, running on %s\n", n_tasks, rank, host_name);
    MPI_Finalize();
}
